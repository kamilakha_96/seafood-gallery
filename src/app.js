import 'bootstrap'
import './css/custom.css'
import './css/app.scss'
import './views/components/top-bar'
import './views/components/bottom-bar'
import './views/components/main-body'
import './views/components/modal-main'
import './views/components/search-bar'

const app = () => {
  const searchSeafoodElement = document.querySelector('search-bar')
  const seafoodListElement = document.querySelector('main-body')

  const onKeyupSearch = () => {
    const seafoodList = seafoodListElement.listMeal
    const filter = searchSeafoodElement.value

    for (let i = 0; i < seafoodList.length; i++) {
      const txtValue = seafoodList[i].getAttribute('title')
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        seafoodList[i].style.display = ''
      } else {
        seafoodList[i].style.display = 'none'
      }
    }
  }
  searchSeafoodElement.search = onKeyupSearch
}

document.addEventListener('DOMContentLoaded', app)
