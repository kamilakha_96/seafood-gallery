class BottomBar extends HTMLElement {
  connectedCallback () {
    this.render()
  }

  render () {
    this.innerHTML = `
        <footer class="fixed-footer">
            <div class="container">
                <h2 class="site-footer-info">
                Copyright &copy;
                ${new Date().getFullYear()}
                Latihan Dicoding - Seafood Gallery</h1>
            </div>
        </footer>
        `
  }
}

customElements.define('bottom-bar', BottomBar)
