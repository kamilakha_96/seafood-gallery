import { Modal } from 'bootstrap'

class ModalMain extends HTMLElement {
  // eslint-disable-next-line accessor-pairs
  set idMeal (id) {
    this._idMeal = id
    this.render()
  }

  render () {
    // eslint-disable-next-line promise/param-names
    return new Promise((res, rej) => {
      const url = `https://www.themealdb.com/api/json/v1/1/lookup.php?i=${this._idMeal}`
      fetch(url)
        .then(response => response.json())
        .then(data => {
          res()
          this.renderPost(data)
        })
        .catch(error => rej(error))
    })
  }

  renderPost (data) {
    this.innerHTML = ''
    const container = document.createElement('div')
    data.meals.forEach(item => {
      const { strMeal, strInstructions, strMealThumb, strTags } = item
      container.innerHTML = `
                <div class="modal" id="seafoodModal" tabindex="-1" aria-labelledby="seafoodModalLabel">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="seafoodModalLabel">${strMeal.toUpperCase()}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <img src="${strMealThumb}" alt="${strMealThumb}" class="center">
                        <p><u>How to Cook: </u></p>
                        <p class="site-instructions">${strInstructions}</p>
                    </div>
                    <div class="modal-footer">
                        <p><b>Tags: ${strTags != null ? strTags : '-'}</b></p>
                    </div>
                    </div>
                </div>
                </div>
                `
    })
    this.appendChild(container)
    const myModal = this.querySelector('#seafoodModal')
    const seafoodModal = new Modal(myModal, {
      keyboard: false
    })
    seafoodModal.show()
  }
}
window.customElements.define('modal-main', ModalMain)
