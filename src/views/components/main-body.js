/* eslint-disable no-undef */
import './modal-main'

class MainBody extends HTMLElement {
  connectedCallback () {
    this.render()
  }

  get nameMeal () {
    return this.querySelector('#list-meal')
  }

  get listMeal () {
    return this.querySelectorAll('.meal')
  }

  render () {
    // eslint-disable-next-line promise/param-names
    return new Promise((res, rej) => {
      const url = 'https://www.themealdb.com/api/json/v1/1/filter.php?c=Seafood'
      fetch(url)
        .then(response => response.json())
        .then(data => {
          res()
          this.renderPost(data)
        })
        .catch(error => rej(error))
    })
  }

  renderPost (data) {
    const listMeal = document.createElement('div')
    $(listMeal).attr({ id: 'list-meal', class: 'container' })
    const mealGroup = document.createElement('div')
    $(mealGroup).attr({ class: 'row' })
    listMeal.innerHTML = ''
    data.meals.forEach(item => {
      const { idMeal, strMeal, strMealThumb } = item
      const modalMain = document.querySelector('modal-main')
      const colMeal = document.createElement('div')
      $(colMeal).attr({ class: 'meal col-12 col-sm-6 col-md-4 col-xl-3 mb-3', title: `${strMeal}` })
      const meal = document.createElement('div')
      $(meal).attr({ class: 'card h-100', id: `${idMeal}`, title: `${strMeal}` })
      meal.innerHTML = `
                <img src="${strMealThumb}" class="card-img-top" alt="${strMealThumb}">
                <div class="card-body">
                    <h5 class="card-title">${strMeal}</h5>
                </div> `
      colMeal.append(meal)
      mealGroup.append(colMeal)
      meal.addEventListener('click', (e) => {
        modalMain.idMeal = e.currentTarget.id
      })
    })
    listMeal.append(mealGroup)
    this.appendChild(listMeal)
  }
}

customElements.define('main-body', MainBody)
