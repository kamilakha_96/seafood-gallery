class TopBar extends HTMLElement {
  connectedCallback () {
    this.render()
  }

  render () {
    this.innerHTML = `
        <div class="container">
            <header class="site-header">
                <h1 class="site-title">SEAFOOD GALLERY</h2>
            </header>
        </div>`
  }
}

customElements.define('top-bar', TopBar)
