class SearchBar extends HTMLElement {
  connectedCallback () {
    this.render()
  }

  // eslint-disable-next-line accessor-pairs
  set search (name) {
    this._search = name
    this.render()
  }

  get value () {
    return this.querySelector('#searchSeafood').value.toUpperCase()
  }

  render () {
    this.innerHTML = `
        <div class="container">
            <div class="row height d-flex justify-content-center align-items-center">
                <div class="col-md-6">
                    <div class="form"><input id="searchSeafood" type="text" class="form-control form-input" placeholder="Search seafood name..." autocomplete="off"> </div>
                </div>
            </div>
        </div>
        `
    this.querySelector('#searchSeafood').addEventListener('keyup', this._search)
  }
}
customElements.define('search-bar', SearchBar)
